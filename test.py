import unittest
from unittest.mock import patch, MagicMock
from datetime import datetime
import pytz
from main import minutes_to_next_hour, get_date, main, check_price_and_send, get_data

@patch('main.BASE_URL', 'http://example.com/')
@patch('main.TIMEZONE', 'Europe/Prague')
class TestMyModule(unittest.TestCase):

    @patch('main.requests.get')
    @patch('main.MAX_MINUTES_LEFT', 10)
    @patch('main.MIN_PRICE', 50)
    def test_main(self, mock_requests_get):      
        # Setup mock response
        mock_response = MagicMock()
        mock_response.json.return_value = {
            'data': {
                'dataLine': [
                    {},
                    {
                        'point': [
                            {'y': 40},  # 0-1h
                            {'y': 60},  # 1-2h
                            {'y': 70},  # 2-3h
                            {'y': 80},  # 3-4h
                        ]
                    }
                ]
            }
        }
        mock_requests_get.return_value = mock_response

        # Mock datetime to control the current time
        mock_now = datetime(2023, 5, 19, 2, 45, tzinfo=pytz.timezone('Europe/Prague'))
        with patch('main.get_now', return_value=mock_now):
            with patch('main.send_turn_on') as mock_send_turn_on:
                with patch('main.send_turn_off') as mock_send_turn_off:
                    # Run main
                    main()

                    # Assertions
                    mock_requests_get.assert_called_once_with('http://example.com/2023-05-19')
                    mock_send_turn_on.assert_called_once()
                    mock_send_turn_off.assert_not_called()

    def test_minutes_to_next_hour(self):
        # Mock datetime
        mock_now = datetime(2023, 5, 19, 14, 45, tzinfo=pytz.timezone('Europe/Prague'))
        with patch('main.get_now', return_value=mock_now):
            minutes_left = minutes_to_next_hour()
            self.assertEqual(minutes_left, 15)

    def test_get_date(self):
        # Mock datetime
        mock_now = datetime(2023, 5, 19, 14, 45, tzinfo=pytz.timezone('Europe/Prague'))
        with patch('main.get_now', return_value=mock_now):
            prague_now, date_formatted = get_date()
            self.assertEqual(date_formatted, '2023-05-19')
            self.assertEqual(prague_now, mock_now)

    @patch('main.MIN_PRICE', 50)
    def test_check_price_and_send(self):
        with patch('main.send_turn_on') as mock_send_turn_on:
            with patch('main.send_turn_off') as mock_send_turn_off:
                check_price_and_send(60)  # Price > MIN_PRICE (50)
                mock_send_turn_on.assert_called_once()
                mock_send_turn_off.assert_not_called()

                mock_send_turn_on.reset_mock()
                mock_send_turn_off.reset_mock()
                
                check_price_and_send(40)  # Price < MIN_PRICE (50)
                mock_send_turn_on.assert_not_called()
                mock_send_turn_off.assert_called_once()

    @patch('main.requests.get')
    @patch('main.get_date')
    def test_get_data_successful_response(self, mock_get_date, mock_requests_get):
        # Mock date and date_formatted
        mock_date = datetime(2023, 5, 19, 2, 0, tzinfo=pytz.timezone('Europe/Prague'))
        mock_get_date.return_value = (mock_date, '2023-05-19')
        
        # Mock API response
        mock_response = MagicMock()
        mock_response.json.return_value = {
            'data': {
                'dataLine': [
                    {},
                    {
                        'point': [
                            {'y': 40},  # 0-1h
                            {'y': 60},  # 1-2h
                            {'y': 70},  # 2-3h
                            {'y': 80},  # 3-4h
                        ]
                    }
                ]
            }
        }
        mock_requests_get.return_value = mock_response

        # Call get_data function
        date, current_hour, next_hour = get_data()

        # Assertions
        self.assertEqual(current_hour, 70)
        self.assertEqual(next_hour, 80)

    @patch('main.requests.get')
    @patch('main.get_date')
    def test_get_data_missing_data(self, mock_get_date, mock_requests_get):
        # Mock date and date_formatted
        mock_date = datetime(2023, 5, 19, 14, 0, tzinfo=pytz.timezone('Europe/Prague'))
        mock_get_date.return_value = (mock_date, '2023-05-19')
        
        # Mock API response with missing data
        mock_response = MagicMock()
        mock_response.json.return_value = {
            'data': {
                'dataLine': [
                    {},
                    {
                        'point': [{'y': 40}]  # Only one data point available
                    }
                ]
            }
        }
        mock_requests_get.return_value = mock_response

        # Call get_data function and expect an exception
        with self.assertRaises(Exception) as context:
            get_data()

        # Assertions
        self.assertTrue('Data not available for 14-15h' in str(context.exception))


    @patch('main.requests.get')
    @patch('main.get_date')
    def test_get_data_for_23h(self, mock_get_date, mock_requests_get):
        # Mock date and date_formatted
        mock_date = datetime(2023, 5, 19, 23, 0, tzinfo=pytz.timezone('Europe/Prague'))
        mock_get_date.return_value = (mock_date, '2023-05-19')
        # Mock API response with missing data
        mock_response = MagicMock()
        mock_response.json.return_value = {
            'data': {
                'dataLine': [
                    {},
                    {
                        'point': [{'y': i} for i in range(1, 25)]  # for all day
                    }
                ]
            }
        }
        mock_requests_get.return_value = mock_response

        # Call get_data function and expect an exception
        _, current_hour, next_hour = get_data()
        self.assertEqual(current_hour, 24)
        self.assertEqual(next_hour, 24)

    @patch('main.requests.get')
    @patch('main.get_date')
    def test_get_data_for_0h(self, mock_get_date, mock_requests_get):
        # Mock date and date_formatted
        mock_date = datetime(2023, 5, 19, 0, 0, tzinfo=pytz.timezone('Europe/Prague'))
        mock_get_date.return_value = (mock_date, '2023-05-19')
        # Mock API response with missing data
        mock_response = MagicMock()
        mock_response.json.return_value = {
            'data': {
                'dataLine': [
                    {},
                    {
                        'point': [{'y': i} for i in range(1, 25)]  # for all day
                    }
                ]
            }
        }
        mock_requests_get.return_value = mock_response

        # Call get_data function and expect an exception
        _, current_hour, next_hour = get_data()
        self.assertEqual(current_hour, 1)
        self.assertEqual(next_hour, 2)

if __name__ == '__main__':
    unittest.main()
