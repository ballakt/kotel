import requests
import pytz
from datetime import datetime
from config import get_config
import logging
import logging.handlers
from systemd import journal

# Create a logger

logger = logging.getLogger('main')
logger.setLevel(logging.INFO)
stream_handler = logging.StreamHandler()
journal_handler = journal.JournalHandler()
formatter = logging.Formatter('[%(asctime)s][%(name)s][%(levelname)s] - %(message)s')
journal_handler.setFormatter(formatter)
stream_handler.setFormatter(formatter)
logger.addHandler(journal_handler)
logger.addHandler(stream_handler)

configObj = get_config()

BASE_URL = configObj['BASE_URL']
MAX_MINUTES_LEFT = configObj['MAX_MINUTES_LEFT']
MIN_PRICE = configObj['MIN_PRICE']
TIMEZONE = configObj['TIMEZONE']
PRICE_CURRENCE = '€/MWh'
Relay_Ch1 = 26
try:
    import RPi.GPIO as GPIO
except:
    import Mock.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

GPIO.setup(Relay_Ch1,GPIO.OUT)

def send_turn_on(reason: str):
    GPIO.output(Relay_Ch1,GPIO.LOW)
    logger.info(f">>> TURN ON ({reason})")

def send_turn_off(reason: str):
    GPIO.output(Relay_Ch1,GPIO.HIGH)
    logger.info(f">>> TURN OFF ({reason})")

def get_now():
    # Get the current UTC time
    utc_now = datetime.utcnow()

    # Localize the current UTC time to UTC timezone
    utc_timezone = pytz.utc
    utc_now = utc_timezone.localize(utc_now)

    # Convert to Europe/Prague timezone
    timezone = pytz.timezone(TIMEZONE)
    timezone_now = utc_now.astimezone(timezone)

    return timezone_now

def minutes_to_next_hour():
    now = get_now()
    current_minute = now.minute
    minutes_left = 60 - current_minute
    return minutes_left

def get_date():
    prague_now = get_now()

    formatted_date = prague_now.strftime('%Y-%m-%d')

    date = formatted_date

    return prague_now, date

def get_data():
    date, date_formatted = get_date()
    resp = requests.get(f'{BASE_URL}{date_formatted}')
    logger.info(f'{BASE_URL}{date_formatted}')
    points = resp.json()['data']['dataLine'][1]['point']
    
    hours_data = [point['y'] for point in points]
    
    next_hour = date.hour + 1
    
    if (len(hours_data) <= date.hour):
        raise Exception(f"Data not available for {date.hour}-{date.hour + 1}h")
    
    if (len(hours_data) <= next_hour):
        if (next_hour == 24):
            next_hour = date.hour
        else:
            raise Exception(f"Data not available for {next_hour}-{next_hour + 1}h")            
    
    current_hour_data = hours_data[date.hour]
    next_hour_data = hours_data[next_hour]

    return date, current_hour_data, next_hour_data, hours_data

def main():
    logger.info("=Beginning==============")
    try:
        date, current_hour, next_hour , hours_data = get_data()

        logger.info(f"{date.hour}-{date.hour + 1}h={current_hour} {PRICE_CURRENCE}")
        logger.info(f"{date.hour + 1}-{date.hour + 2}h={next_hour} {PRICE_CURRENCE}")
        
        from_0_to_14 = hours_data[0:14]
        minutes_left_to_next_hour = minutes_to_next_hour()
        logger.info(f"{minutes_left_to_next_hour} minutes left to next hour; max {int(MAX_MINUTES_LEFT)}m")

        # Find minus value from 0:00 - 14:00
        if (any(value <= MIN_PRICE for value in from_0_to_14)):
            # Is current hour less than 14h
            first_minus_hour = -1
            for index, value in enumerate(from_0_to_14):
                if (value <= MIN_PRICE):
                    first_minus_hour = index
                    break

            hour_check = date.hour + 1 if date.hour != 23 and minutes_left_to_next_hour <= MAX_MINUTES_LEFT  else date.hour

            logger.info(f">>> {hour_check}-{hour_check+1}h")
            if (hour_check < first_minus_hour):
                # Turn it off we will turn it on after first minus hour
                send_turn_off(f"waiting for {first_minus_hour}h")
            else:
                send_turn_on(f"price {current_hour}{PRICE_CURRENCE} is under {MIN_PRICE}{PRICE_CURRENCE}")
        else:
            send_turn_on(f"no price under {MIN_PRICE} today")
    except Exception as e:
        logger.error(e)
        send_turn_on()
        raise e
    logger.info("=End====================")
main()