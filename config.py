import os
from dotenv import load_dotenv
from jsonschema import validate, ValidationError
# Load the .env file
load_dotenv()

# Define the JSON schema
schema = {
    "type": "object",
    "properties": {
        "BASE_URL": {"type": "string"},
        "MAX_MINUTES_LEFT": {"type": "number"},
        "MIN_PRICE": {"type": "number"},
        "TIMEZONE": {"type": "string"}
    },
    "required": ["BASE_URL", "MAX_MINUTES_LEFT", "MIN_PRICE", "TIMEZONE"]
}

# Function to load and validate environment variables
def load_and_validate_config(schema):
    config = {
        "BASE_URL": os.getenv("BASE_URL"),
        "MAX_MINUTES_LEFT": os.getenv("MAX_MINUTES_LEFT"),
        "MIN_PRICE": os.getenv("MIN_PRICE"),
        "TIMEZONE": os.getenv("TIMEZONE")
    }

    # Convert numeric values
    try:
        config["MAX_MINUTES_LEFT"] = float(config["MAX_MINUTES_LEFT"])
        config["MIN_PRICE"] = float(config["MIN_PRICE"])
    except (ValueError, TypeError) as e:
        raise ValueError(f"Error converting environment variables to numeric values: {e}")

    # Validate against schema
    try:
        validate(instance=config, schema=schema)
    except ValidationError as e:
        raise ValueError(f"Configuration validation error: {e.message}")

    return config

def get_config():
    # Load and validate configuration
    try:
        config = load_and_validate_config(schema)
        # logger.info("==============")
        # logger.info(f"* BASE_URL: {config['BASE_URL']}")
        # logger.info(f"* MAX_MINUTES_LEFT: {config['MAX_MINUTES_LEFT']}")
        # logger.info(f"* MIN_PRICE: {config['MIN_PRICE']}")
        # logger.info(f"* TIMEZONE: {config['TIMEZONE']}")
        # logger.info("/=============")
        return config
    except ValueError as e:
        print(f"Error: {e}")

